# U08 laC and CM for Continous Deployment
Contributors: Sem Haile & Selim Hussen

# The files needed for setting everything up
- main.tf
- inventory.yml
- caprover-playbook.yml
- README.md

Make sure that you have all the provided files in the same directory

# elastx

- When signed in, head over to API access

![image.png](./screenshots/image.png)

Click on the icon that says " Download OpenStack RC file" and choose "Openstack RC file"

Move the RC file to the same directory as the files provided in the previous step

# Setting up infrastructure


Step 1
- Open up the directory with a editor of your choise (i chose VScode for this)
- In the terminal run 'source chasacademy-haile-openrc.sh' (Change the source file name to whatever yours is)
- Type your password and enter

Step 2

In the same terminal simply type
- Terraform init
- Terraform apply

You should now have 2 instances with 2 floating IPs up and running!
![image-2.png](./screenshots/image-2.png)
- Copy your floating IPs and paste them as the shown below
![image-1.png](./screenshots/image-1.png)

# Caprover Cluster
To create a cluster you'll need a so called swarm. We'll simply just SSH in to the instances and add them manually 

Make sure that you have docker installed

We will use server 1 as manager node, so once you're in just type
- sudo docker swarm join-token worker

Copy the output from Server 1 and SSH in to Server 2. Once you're in just paste the output and hit enter

If you head over to your Caprover it will show your cluster

(Example below)

![image-3.png](./screenshots/image-3.png)

# Gitlab CI

1. Create a Gitlab Repository

2. Add the .php file , Dockerfile, and gitlab-ci.yml-file included in the repo

3. Create your CI/CD variables as shown below
![image-4.png](./screenshots/image-4.png)

4. Head over to User settings > Access token and create a token with read_registry and write_registry permissions
![image-5.png](./screenshots/image-5.png)

5. Add Token to CapRover Login to your CapRover web dashboard, under Cluster click on Add Remote Registry.
- User name = Gitlab Username
- Gitlab Token = Token generated in previous step
- In the domain section add: registry.gitlab.com
- Skip the image prefix part

6. Change the default Docker registry to your Gitlab registry
![image.png](./screenshots/image-6.png)

7. Navigate to Create app on Caprover and create an app with the same variable as you named in step 3

8. Push to your repo and wait a few minutes and your app should be up and running
